<?php
header("Access-Control-Allow-Origin: *");
// API key gor Google Places
$places_api_key = "AIzaSyD9DlbkD-zWWMSCjjKwkZSOOa8Y_4pXEUo";
// Foursquare API id
$foursquare_id = "FZIQUGWI43WQ0XYPG43MXDJGNK4MDTTS3X1XDMSJVXDENHMV";
// Foursquare API secret
$foursquare_secret = "2FR5JJ0J1LECYH2OA40HSKGU4C515I4DGEDUBFC0BZLX4GEY";
// Foursquare version date
$foursquare_version = "20190131";
// get the category (default is "arts")
$category = isset($_GET['category'])?$_GET['category']:"arts";
// url to get json data from Foursquare
$url = "https://api.foursquare.com/v2/venues/explore?near=vaxjo&client_id=".$foursquare_id."&client_secret=".$foursquare_secret."&v=".$foursquare_version."&section=".$category;

// function that takes a url to retrieve json data,
// gets contents of json data from that url
// and returns array of decoded json data
function get_data($json_url){
	$json = file_get_contents($json_url,true);
	$result = json_decode($json);
	return $result;
}
// extract relevant data from Foursquare
$extract = get_data($url)->response->groups[0]->items;
// create an array to hold data about places
$foursquare_data = array();
// initialte index
$i = 0;
// loop through Foursquare data
// and add needed data to the array
foreach($extract as $obj)
{
	$foursquare_data[$i] = array(
	// latitude
	"lat" => $obj->venue->location->lat,
	// longitude
	"lon" => $obj->venue->location->lng,
	// name of the place
	"name" => $obj->venue->name,
	// place category
	"cat" => $obj->venue->categories[0]->shortName,
	);
	// venue id (used to retrieve additional information about that venue)
	$venue_id = $obj->venue->id;
	// url to get additional info from Foursquare
	$f_photo_url = "https://api.foursquare.com/v2/venues/".$venue_id."/photos?limit=1&client_id=".$foursquare_id."&client_secret=".$foursquare_secret."&v=".$foursquare_version;
	// retrieve data about a photo for the place
	$f_photo_data = get_data($f_photo_url);
	// build path/url for that photo
	// if there is no photo available - provide url to an alternative image
	$f_photo = ($f_photo_data->response->photos->count == 1)?$f_photo_data->response->photos->items[0]->prefix."width200".$f_photo_data->response->photos->items[0]->suffix:"https://4me304.000webhostapp.com/assets/img/no_image.jpg";
	// store photo url into array
	$foursquare_data[$i]["f_photo"] = $f_photo;
	// create query details in appropriate format
	$place_formatted = preg_replace('/\s+/', '%20', $foursquare_data[$i]['name'])."%20Vaxjo";

	$location = "&locationbias=circle:200@".$foursquare_data[$i]['lat'].",".
	$foursquare_data[$i]['lon'];
	// url to get Google places id based on the given coordinates and the venue's name from Google places about the venue
	$place_url = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=".$place_formatted."&inputtype=textquery".$location."&key=".$places_api_key;

	// decode Google place id data
	$g_place_id = get_data($place_url);

	// retrieve Google place id
	$g_id = $g_place_id->candidates[0]->place_id;

	// url to get json data from Google places containing additional details based on Google place id
	$place_details_url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=".$g_id."&fields=name,formatted_phone_number,formatted_address,opening_hours/weekday_text&key=".$places_api_key;
	// decode json data from Google places details
	$place_details = get_data($place_details_url);
	// add data to array
	// Google place name
	$foursquare_data[$i]["g_name"] = $place_details->result->name;
	//address
	$foursquare_data[$i]["address"] = $place_details->result->formatted_address;
	// phone number
	$foursquare_data[$i]["phone"] = isset($place_details->result->formatted_phone_number)?$place_details->result->formatted_phone_number:"Not Provided";
	// opening hours (if any)
	$foursquare_data[$i]["hours"] = isset($place_details->result->opening_hours)?$place_details->result->opening_hours->weekday_text:array("Not available");
	// increase index
	$i++;

}
// return array as json
print_r(json_encode($foursquare_data));
?>
