import { Component, OnInit } from '@angular/core';
import {TwitterService} from '../services/twitter.service';

@Component({
  selector: 'app-twitter',
  templateUrl: './twitter.component.html',
  styleUrls: ['./twitter.component.scss']
})
export class TwitterComponent implements OnInit {
  tweets: Array<any> = [];

  constructor(private twitterService: TwitterService) {}

  ngOnInit() {
    this.twitterService.getTweets()
      .subscribe(
        response => {
          this.tweets = (response as any).statuses;
        },
        error => console.error(error)
      );
  }


}
