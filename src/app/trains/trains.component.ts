import { Component, OnInit } from '@angular/core';
import {TrainsService} from '../services/trains.service';
import {$} from 'protractor';
import {filter} from 'rxjs/operators';
import {from} from 'rxjs';
import {SmhiService} from '../services/smhi.service';

@Component({
  selector: 'app-trains',
  templateUrl: './trains.component.html',
  styleUrls: ['./trains.component.scss']
})
export class TrainsComponent implements OnInit {
  
  updatedDepartures: Array<any> = [];
  departures: Array<any> = [];
  stations: Array<any> = [];
  startStation = 315; // id for växjö station
  station = 'Kalmar';
  noResult = false;

  constructor(private trainsService: TrainsService, private smhiService: SmhiService) { }

  ngOnInit() {
    // get all stations
    this.trainsService.getStations()
      .subscribe(
        response => {
          this.stations = (response as any).stations.station;
        },
        error => console.error(error)
      );
    // get all departures from a given station e.g. Växjö
    this.trainsService.getStationDepartures(this.startStation)
      .subscribe(
        response => {
          this.departures = (response as any).station.transfers.transfer;
        },
        error => console.error(error)
      );
  }
  // perform search for destination stations based on user input
  // check if we have a result or the boolean no result is set to true
  onSearch(stationInput: HTMLInputElement) {
    this.noResult = true;
    this.updatedDepartures = [];
    const source = from(this.departures);
    const station = source.pipe(filter(departure =>
      departure.destination.toLocaleLowerCase().indexOf(stationInput.value.toLocaleLowerCase()) !== -1));
    station.subscribe(val => {
      this.updatedDepartures.push(val);
      this.noResult = this.updatedDepartures.length === 0;
    });
  }
}
