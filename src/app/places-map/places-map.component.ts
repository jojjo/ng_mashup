import { Component, OnInit } from '@angular/core';
import {PlacesService} from "../services/places.service";

@Component({
  selector: 'app-places-map',
  templateUrl: './places-map.component.html',
  styleUrls: ['./places-map.component.scss']
})
export class PlacesMapComponent implements OnInit {
  DEFAULT_MAP_ZOOM = 13;
  DEFAULT_MAP_LAT = 56.8770413;
  DEFAULT_MAP_LNG = 14.8092744;
  // path to map marker icon (Foursquare)
  markerUrl = 'https://4me304.000webhostapp.com/assets/img/blue_marker.png';
  // array to hold Foursquare places' categories
  categories = ['arts', 'coffee', 'food', 'drinks', 'outdoors', 'sights', 'shops'];
  // array to hold Foursquare places markers
  placesMarkers: Array<any> = [];
  // default category for Foursquare places
  category = 'arts';
  infoWindowOpened = null;
  previous_info_window = null;

  constructor(private placesService: PlacesService) { }

  ngOnInit() {
    // place default category places from Foursquare on the map
    this.showPlaces();
  }

  // function to retrieve places /  venues data from Foursquare service
  showPlaces() {
    this.placesService.getPlaces(this.category).subscribe(data=>{
      console.log(data)
      const places = data;
      // iterate through array and append places markers to the map
      for (const place of places) {
        this.appendPlacesToMap(place);
      }
    });

  }

  // function to create and populate markers array for Foursquare places with additional information from Google places
  appendPlacesToMap(place) {
    // store place's latitude
    const latit = Number(place.lat);
    // store place's longitude
    const longi = Number(place.lon);
    // create and populate markers array for places
    const placeMarker = {lat: latit, lang: longi, title: place.name, addr: place.address,
      icon: this.markerUrl, cat: place.cat, phone: place.phone, img: place.f_photo, hours: place.hours};
    this.placesMarkers.push(placeMarker);
  }
  // function to retrieve places /  venues data from Foursquare service
  /*showPlaces() {
    this.foursquareService.getPlaces('&section=' + this.category).subscribe((res)=>{
      console.log(res.response.groups[0].items);
      // create an array to hold places relevant data
      const places = res.response.groups[0].items;
      // iterate through array and append places markers to the map
      for (const place of places) {
        this.appendPlacesToMap(place);
      }
    });

  }
  // function to create and populate markers array for Foursquare places
  appendPlacesToMap(place) {
    // store place's latitude
    const latit = Number(place.venue.location.lat);
    // store place's longitude
    const longi = Number(place.venue.location.lng);
    // create and populate markers array for places
    const placeMarker = {lat: latit, lang: longi, title: place.venue.name, addr: place.venue.location.address,
      icon: this.markerUrl, cat: place.venue.categories[0].shortName};
    this.placesMarkers.push(placeMarker);
  }*/

  // function to handle onSelect event for a selector form element
  onSelectChange() {
    // clear array (to display a new query)
    this.placesMarkers.length = 0;
    //refresh all markers variables
    this.infoWindowOpened = null;
    this.previous_info_window = null;
    // place relevant places markers on the map
    this.showPlaces();
  }

  // two functions below ensure that if info window for one marker is open and another marker is clicked
  // or if the user clickes on the map, that the previously opened info windows will be closed
  // the code is courtesy of https://stackoverflow.com/questions/47110030/close-the-previous-infowindow-with-angular-map-agm
  closeWindow(){
    if (this.previous_info_window != null ) {
      this.previous_info_window.close();
    }
  }

  selectMarker(infoWindow){
    if (this.previous_info_window == null)
      this.previous_info_window = infoWindow;
    else{
      this.infoWindowOpened = infoWindow;
      this.previous_info_window.close();
    }
    this.previous_info_window = infoWindow;
  }


}
