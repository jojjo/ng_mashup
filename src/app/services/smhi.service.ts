import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SmhiService {
 private smhiServiceHttpClient: HttpClient;

  constructor(http: HttpClient) {
    this.smhiServiceHttpClient = http;
  }
  getArrivalWeather(lat, long) {
    const weather = 'https://opendata-download-metfcst.smhi.se/api/category/pmp3g/version/2/geotype/point/lon/'
      + long + '/lat/' + lat + '/data.json';
    return this.smhiServiceHttpClient.get(weather);
  }
}
