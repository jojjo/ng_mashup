import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FoursquareService {
  // Foursquare client id
  clientId = 'FZIQUGWI43WQ0XYPG43MXDJGNK4MDTTS3X1XDMSJVXDENHMV';
  // Foursquare client secret
  clientSecret = '2FR5JJ0J1LECYH2OA40HSKGU4C515I4DGEDUBFC0BZLX4GEY';
  // Foursquare version date
  version = '20181231';
  // url to retrieve information from Foursquare (explore places near Vaxjo)
  url = 'https://api.foursquare.com/v2/venues/explore?near=vaxjo&client_id=' + this.clientId
    + '&client_secret=' + this.clientSecret + '&v=' + this.version;

  constructor(private http: HttpClient) { }
  //function to return json data from query to Foursquare api
  //variable data is used to access different categories of places
  getPlaces(data: string): Observable<any> {
    return this.http.get(this.url + data);
  }
}
