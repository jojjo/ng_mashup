import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {forEach} from '@angular/router/src/utils/collection';

@Injectable({
  providedIn: 'root'
})
export class TrainsService {
  private trainServicesHttpClient: HttpClient;

  constructor(http: HttpClient) {
    this.trainServicesHttpClient = http;
  }

  getStationDepartures(id) {
    const departures = 'https://us-central1-mytestproject-e7673.cloudfunctions.net' +
      '/tagrider/v1/stations/' + id + '/transfers/departures.json';
    return this.trainServicesHttpClient.get(departures);
  }

  getStations() {
    const getStations = 'https://us-central1-mytestproject-e7673.cloudfunctions.net' +
      '/tagrider/v1/stations.json';
    return this.trainServicesHttpClient.get(getStations);
  }
}
