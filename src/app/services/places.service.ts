import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class PlacesService {

  //url = 'https://4me304.000webhostapp.com/assets/php/data.json';
  url2 = './assets/data.json';

  constructor(private http: HttpClient) { }

  getPlaces(data: string): Observable<any> {
    const url = 'https://4me304.000webhostapp.com/assets/php/GooglePlacesFoursquareService.php?category=' + data;
    return this.http.get(url);
  }
}
