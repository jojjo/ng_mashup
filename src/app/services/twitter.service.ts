import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TwitterService {
  // twitter consumer key
  consumerKey = 'IqY4FEXmWAcMJ8xcDz4smRRYT';
  // twitter consumer secret
  consumerSecret = 'Jq4ICJaUOLIVgok3Fs8YivLhigvvJ6AqNf8XRGSXeiRFPRJjet';
  // url encoded consumer key
  resConsumerKey = encodeURI('IqY4FEXmWAcMJ8xcDz4smRRYT');
  // url encoded consumer secret
  resConsumerSecret = encodeURI('Jq4ICJaUOLIVgok3Fs8YivLhigvvJ6AqNf8XRGSXeiRFPRJjet');
  // concatenated url encoded consumer key and secret
  bearerTokenCredential = this.resConsumerKey + ':' + this.resConsumerSecret;
  // base 64 encoded concatenated url encoded consumer key and secret
  base64 = btoa(this.bearerTokenCredential);
  // api url to make request to twitter
  apiUrl = 'https://api.twitter.com/oauth2/token';


  constructor(private http: HttpClient) {
  }
  // api call to get all tweets created around Växjö with 25 km radius
  getTweets() {
    const url = 'https://us-central1-mytestproject-e7673.cloudfunctions.net/twitterrider/';
    const geoQueryString = '?geocode=56.879005,14.805852,25km';
    return this.http.get(url + geoQueryString);
  }
  getBearer() {
    const formData = new FormData();
    formData.append('grant_type', 'client_credentials');

    this.http.post(this.apiUrl, formData,
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': 'Basic ' + this.base64,
        })
      }
    )
      .subscribe(
        (res) => {
          console.log(res);
        },
        err => console.log(err)
      );
  }
}
